#include "arvore.h"
#include <stdio.h>
#include <stdlib.h>

/* Inicializa uma árvore
 * @param entradas número de valores iniciais
 * @param valores vetor com valores a serem inseridos
 * @return raiz da árvore ou NULL se erro
 */ 

struct nodo * inicializa_arvore(int entradas, int * valores){
	struct nodo *raizum;
	int i=0;
	if(entradas<=0 || valores==NULL)
	{
		return NULL;
	}
		raizum=malloc(sizeof(struct nodo));
		raizum->valor=valores[0];
		raizum->dir=NULL;
		raizum->esq=NULL;
		for (i =1; i< entradas;i++){
			insere_nodo(raizum,valores[i]);
		}
		return raizum;
};

/* Insere nodo em uma árvore se a chave ainda não existir, sempre como uma folha
 * @param raiz raiz da árvore
 * @param valor o que será inserido 
 * @return raiz da árvore ////////////////como??????
 */ 
struct nodo * insere_nodo(struct nodo * raiz, int valor){

	if (raiz==NULL)
	{
		struct nodo *aux;
		aux=malloc(sizeof(struct nodo));
		if(aux==NULL){
			exit(1);
		}
		aux->valor=valor;
		aux->dir= NULL;
		aux->esq=NULL;
		return aux;
	}
	
	if (valor < raiz->valor)
	{
		raiz->esq = insere_nodo(raiz->esq,valor);
		return raiz;
	}
	if (valor > raiz->valor)
	{
		raiz->dir = insere_nodo(raiz->dir,valor);
		return raiz;
	}
	return raiz;
}

/* Remove nodo em uma árvore se existir, dando prioridade à subárvore esquerda para novo nodo raiz
 * @param raiz raiz da árvore
 * @param valor o que será removido 
 * @return raiz da árvore
 */ 

struct nodo * remove_nodo(struct nodo * raiz, int valor){
    struct nodo *aux;
	if (raiz==NULL)
	{
		return NULL;
	}
	else{  
		if (raiz->valor>valor)
		{
		raiz->esq=remove_nodo(raiz->esq, valor);
		}	
		else{
			 if (raiz->valor<valor)
			{
				raiz->dir=remove_nodo(raiz->dir,valor);
			}

			else{
				if (raiz->dir!=NULL && raiz->esq !=NULL)
				{
					aux=raiz->dir;
					while (aux->esq!=NULL)
					{
						aux=aux->esq;
					}
					raiz->valor=aux->valor;
					raiz->dir=remove_nodo(raiz->dir,raiz->valor);
				}
				else{
					if (raiz->esq==NULL)
						{
							aux=raiz->dir;
						}
					else
					{
						aux=raiz->esq;
					}
					free (raiz);
					return aux;
				}
			}	
		}
	}
	return raiz; 
}

//*/
/* Altura de uma árvore
 * @param raiz raiz da árvore
 * @return altura da árvore
 */
int max (int a, int b){
	if (a>b)
	{
		return a;
	}
		return b;
}

int altura(struct nodo * raiz){
	if (raiz==NULL)
	{
		return 0;
	}
	return 1+max(altura(raiz->esq), altura(raiz->dir));
		
}

/* Consulta se nodo existe
 * @param raiz raiz da árvore 
 * @param valor chave a ser buscada
 * @return ponteiro para nodo, ou NULL se inexistente
 */
struct nodo * busca(struct nodo * raiz, int valor){
	if (raiz==NULL)
	{
		return NULL;
	}
	if (raiz->valor==valor)
	{
		return raiz;
	}
	if (raiz->valor > valor )
	{
		return busca(raiz->esq,valor);
	}
	if (raiz->valor < valor )
	{
		return busca(raiz->dir, valor);
	}
	return NULL;	
}

/* Descobre se uma árvore está balanceada
 * @param raiz da árvore
 * @return diferença das alturas das subárvores
 */
int balanceada(struct nodo * raiz){
	int menor=0, maior=0, dif=0;
	menor=altura(raiz->esq);
	maior=altura(raiz->dir);
	if (menor==maior)
	{
		return 0;
	}
	else
	{
		dif=maior-menor;
		if (dif>0)
		{
			return dif;
		}
		else
		{
			dif=dif*(-1);
			return dif;
		}
	}
	return 0;
}


/* Número de elementos em uma árvore 
 * @param raiz raiz da árvore
 * @return número de elementos da árvore 
 */
int numero_elementos(struct nodo * raiz){
	if(raiz ==NULL)
	return 0;

	return 1+numero_elementos(raiz->dir)+numero_elementos(raiz->esq);
	
}

/* Percorre a árvore em abrangência 
 * @param raiz raiz da árvore
 * @param resultado vetor onde será armazenado o percurso (já deve estar alocado)
 * @return número atual de elementos no vetor
 */
int abrangencia(struct nodo * raiz, int * resultado){
	int i=0, j=0, tamanho;
	tamanho=numero_elementos(raiz);
	struct nodo *vector[tamanho];
	
	vector[i]=raiz;
	for(j=0; j<tamanho;j++){
		if (vector[j]->esq!=NULL)
		{
			i++;
			vector[i]=vector[j]->esq;
			
		}
		if (vector[j]->dir!=NULL)
		{
			i++;
			vector[i]=vector[j]->dir;
			
		}
		resultado[j]=vector[j]->valor;

	}

	return j;
	
}
/* Percorre a árvore de forma pré-fixada 
 * @param raiz raiz da árvore
 * @param resultado vetor onde será armazenado o percurso (já deve estar alocado)
 * @return número atual de elementos no vetor
 */
int prefix(struct nodo * raiz, int * resultado){
	static int conti=0; 
	if (raiz==NULL)
	{
		return conti;
	}
	resultado[conti]=raiz->valor;
	conti++;
	prefix(raiz->esq, resultado);
	prefix(raiz->dir, resultado);
	return conti;
}

/* Percorre a árvore de forma pós-fixada 
 * @param raiz raiz da árvore
 * @param resultado vetor onde será armazenado o percurso (já deve estar alocado)
 * @return número atual de elementos no vetor
 */
int postfix(struct nodo * raiz, int * resultado){
	static int cont=0; 
	if (raiz==NULL)
	{
		return cont;
	}
	postfix(raiz->esq, resultado);
	postfix(raiz->dir, resultado);
	resultado[cont]=raiz->valor;
	cont++;
	return cont;
}

/* Percorre a árvore de forma infix 
 * @param raiz raiz da árvore
 * @param resultado vetor onde será armazenado o percurso (já deve estar alocado)
 * @return número atual de elementos no vetor
 */
int infix(struct nodo * raiz, int * resultado){
	static int contadori=0;
	if (raiz==NULL)
	{
		return contadori;
	}
	infix(raiz->esq, resultado);
	resultado[contadori]=raiz->valor;
	contadori++;
	infix(raiz->dir, resultado);
	return contadori;	
}

/* Imprime na stdio os valores de um caminhamento com um espaço entre cada valor, 
 * máximo de 10 valores por linha
 * @param valores vetor com valores a serem impressos
 * @param tamanho número de entradas no vetor de valores
 */
void imprime(int * valores, int tamanho){
	
	for (int i = 0; i < tamanho; i++)
	{
		printf("%d\t", valores[i]);
	}
	
}

/* Remove todos os nodos
 * @param raiz da árvore
 */
void remove_todos(struct nodo * raiz){
	if (raiz!=NULL && raiz->dir==NULL && raiz->esq==NULL)
	{
		free(raiz);
	}
	if (raiz!=NULL && raiz->dir!=NULL && raiz->esq==NULL)
	{
		remove_todos(raiz->dir);
		free(raiz);
	}
	if (raiz!=NULL && raiz->dir==NULL && raiz->esq!=NULL)
	{
		remove_todos(raiz->esq);
		free(raiz);
	}
	if (raiz!=NULL && raiz->dir!=NULL && raiz->esq!=NULL)
	{
		remove_todos(raiz->esq);
		remove_todos(raiz->dir);
		free(raiz);
	}
}



